Rails.application.routes.draw do
  get 'sms/send/:to/:message' => 'sms#send'

  post 'autopilot/empanadas' => 'autopilot#empanadas'

  post 'autopilot/reply' => 'autopilot#reply'

  # this url is a twilio webhook
  # go to https://www.twilio.com/console/phone-numbers/incoming
  # click on the phone number
  # on the messaging section set the webhook, the method is POST
  # e.g. linceluchebot.ciit.mx/sms/reply
  post 'sms/reply' => 'sms#reply'

  # this url is a twilio webhook
  # go to https://www.twilio.com/console/phone-numbers/incoming
  # click on the phone number
  # on the messaging section set the webhook, the method is POST
  # e.g. linceluchebot.ciit.mx/sms/reply
  post 'sms/incoming' => 'notifications#incoming'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
