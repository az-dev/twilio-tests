class AutopilotController < ApplicationController
  def reply
    logger.debug params 

    info = 'bla bla bla'

    data = <<-MESSAGE
      {
        "actions": [
          {
            "say": "#{info}"
          },
          {
            "listen": true
          }
        ]
      }
    MESSAGE

    render :json => data
  end

  def empanadas 
    logger.debug params 

    info = "respuesta desde el servidor "

    if params['Field_Type_Value'] == 'mole verde'
      a = Subscriber.first
      info << 'tu pedido está en 20 min '
      info << a.phone_number
    else
      info << 'tu pedido está en 5 min'
    end

    data = <<-MESSAGE
      {
        "actions": [
          {
            "say": "#{info}"
          },
          {
            "listen": true
          }
        ]
      }
    MESSAGE

    render :json => data
  end
end
