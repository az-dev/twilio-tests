class SmsController < ApplicationController
  def send
    account_sid = Rails.application.credentials.twilio[:account_sid]
    auth_token = Rails.application.credentials.twilio[:auth_token]

    to = params[:to]
    message = params[:message]

    client = Twilio::REST::Client.new(account_sid, auth_token)
    client.messages.create(
      from: Rails.application.credentials.twilio[:phone_number],
      to: to,
      body: message
    )
  end

  def reply
    account_sid = Rails.application.credentials.twilio[:account_sid]
    auth_token = Rails.application.credentials.twilio[:auth_token]

    to = params['From']
    message = "Reply back, #{to}! You wrote «#{params['Body']}»"

    client = Twilio::REST::Client.new(account_sid, auth_token)
    client.messages.create(
      from: Rails.application.credentials.twilio[:phone_number],
      to: to,
      body: message
    )
  end
end
