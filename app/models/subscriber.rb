class Subscriber < ApplicationRecord
  def send_message(info)
    account_sid = Rails.application.credentials.twilio[:account_sid]
    auth_token = Rails.application.credentials.twilio[:auth_token]

    client = Twilio::REST::Client.new(account_sid, auth_token)
    client.messages.create(
      from: Rails.application.credentials.twilio[:phone_number],
      to: self.phone_number,
      body: info
    )
  end
end
